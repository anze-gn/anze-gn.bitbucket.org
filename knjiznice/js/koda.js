
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

	switch (stPacienta) {
		case 1:
			ehrId = kreirajEHR("Marija", "Kovačič", "1950-01-10")
			meritve = [
			{	datumInUra : "2016-01-20T09:00",   telesnaVisina : "160",   telesnaTeza : "80",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "140",   diastolicniKrvniTlak : "90",   merilec : "Marija Kovačič"},
			{	datumInUra : "2016-02-20T09:00",   telesnaVisina : "160",   telesnaTeza : "77",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "135",   diastolicniKrvniTlak : "88",   merilec : "Marija Kovačič"},
			{	datumInUra : "2016-03-20T09:00",   telesnaVisina : "160",   telesnaTeza : "75",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "138",   diastolicniKrvniTlak : "86",   merilec : "Marija Kovačič"},
			{	datumInUra : "2016-04-20T09:00",   telesnaVisina : "159",   telesnaTeza : "76",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "133",   diastolicniKrvniTlak : "85",   merilec : "Marija Kovačič"},
			{	datumInUra : "2016-05-20T09:00",   telesnaVisina : "159",   telesnaTeza : "73",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "135",   diastolicniKrvniTlak : "83",   merilec : "Marija Kovačič"}			
			]
			zdravila = [
			{	IDZdravila : "Pc50Xs7PVR1UYvVbD3Cbpn3XYoO", NazivZdravila : "Controloc 20 mg gastrorezistentne tablete",
				ZacetekUporabe : "2016-01-20", KonecUporabe : "2016-02-20",
				Zdravnik : "Dr. Horvat", NavodilaZdravnika : "zjutraj na tešče, po eno tableto na dan"},
			{	IDZdravila : "H39OAEdbm3NV7bLir0fkA5FutmR", NazivZdravila : "Budenofalk 9 mg gastrorezistentna zrnca",
				ZacetekUporabe : "2016-02-20", KonecUporabe : "2016-03-20",
				Zdravnik : "Dr. Horvat", NavodilaZdravnika : "3 tablete na dan"},
			{	IDZdravila : "A9UhvrM5u8NPCv6qZI2pIXHCEtL", NazivZdravila : "Ampril 1,25 mg tablete",
				ZacetekUporabe : "2016-03-20", KonecUporabe : "2016-04-20",
				Zdravnik : "Dr. Horvat", NavodilaZdravnika : "1x na dan, zvečer"},
			{	IDZdravila : "Wh8dvkLwrtPEzgk3lPuz7wn8eC7", NazivZdravila : "Naklofen duo 75 mg kapsule",
				ZacetekUporabe : "2016-01-20", KonecUporabe : "2016-02-20",
				Zdravnik : "Dr. Horvat", NavodilaZdravnika : "po potrebi (1 kapsula, 2x na dan)"},
			]
			break;
		case 2:
			ehrId = kreirajEHR("Franc", "Horvat", "1980-02-20")
			meritve = [
			{	datumInUra : "2015-01-20T09:00",   telesnaVisina : "180",   telesnaTeza : "80",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "120",   diastolicniKrvniTlak : "60",   merilec : "Franc Horvat"},
			{	datumInUra : "2015-10-20T09:00",   telesnaVisina : "179",   telesnaTeza : "77",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "125",   diastolicniKrvniTlak : "66",   merilec : "Franc Horvat"},
			{	datumInUra : "2016-01-20T09:00",   telesnaVisina : "178",   telesnaTeza : "75",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "125",   diastolicniKrvniTlak : "65",   merilec : "Franc Horvat"},
			{	datumInUra : "2016-06-20T09:00",   telesnaVisina : "177",   telesnaTeza : "76",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "130",   diastolicniKrvniTlak : "68",   merilec : "Franc Horvat"},
			{	datumInUra : "2016-12-20T09:00",   telesnaVisina : "177",   telesnaTeza : "73",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "131",   diastolicniKrvniTlak : "69",   merilec : "Franc Horvat"}			
			]
			zdravila = [
			{	IDZdravila : "2uR2kpi8YsW32RdBPVpZlqDWrCj", NazivZdravila : "Plivit D3 4000 i.e./ml peroralne kapljice, raztopina",
				ZacetekUporabe : "2016-01-20", KonecUporabe : "2016-02-20",
				Zdravnik : "Dr. Novak", NavodilaZdravnika : "kapljice, 1x na teden"},
			{	IDZdravila : "GKt3jSElWFr5QDwi1TCQ4y0lq0q", NazivZdravila : "Nolpaza 20 mg gastrorezistentne tablete",
				ZacetekUporabe : "2016-02-20", KonecUporabe : "2016-03-20",
				Zdravnik : "Dr. Novak", NavodilaZdravnika : "1 tableta na tešče"},
			{	IDZdravila : "O1MeY8aGfvxoChU7tm8dEM3X51g", NazivZdravila : "Concor 10 mg filmsko obložene tablete",
				ZacetekUporabe : "2016-03-20", KonecUporabe : "2016-04-20",
				Zdravnik : "Dr. Novak", NavodilaZdravnika : "5mg zjutraj"},
			{	IDZdravila : "2EfbRv0TyC6UQqP42rHCOIw3SGL", NazivZdravila : "Hepan 500 i.e./g / 5 mg/g gel",
				ZacetekUporabe : "2016-01-20", KonecUporabe : "2016-02-20",
				Zdravnik : "Dr. Novak", NavodilaZdravnika : "po potrebi"},
			]
			break;
		case 3:
			ehrId = kreirajEHR("Ana", "Novak", "2010-01-20")
			meritve = [
			{	datumInUra : "2011-01-20T09:00",   telesnaVisina : "100",   telesnaTeza : "10",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "120",   diastolicniKrvniTlak : "90",   merilec : "Marija Novak"},
			{	datumInUra : "2012-02-20T09:00",   telesnaVisina : "110",   telesnaTeza : "20",   telesnaTemperatura : "37.5",
				sistolicniKrvniTlak : "125",   diastolicniKrvniTlak : "88",   merilec : "Marija Novak"},
			{	datumInUra : "2013-03-20T09:00",   telesnaVisina : "115",   telesnaTeza : "25",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "128",   diastolicniKrvniTlak : "86",   merilec : "Marija Novak"},
			{	datumInUra : "2014-04-20T09:00",   telesnaVisina : "120",   telesnaTeza : "28",   telesnaTemperatura : "38.5",
				sistolicniKrvniTlak : "123",   diastolicniKrvniTlak : "85",   merilec : "Marija Novak"},
			{	datumInUra : "2015-05-20T09:00",   telesnaVisina : "122",   telesnaTeza : "29",   telesnaTemperatura : "36.5",
				sistolicniKrvniTlak : "125",   diastolicniKrvniTlak : "83",   merilec : "Marija Novak"}			
			]
			zdravila = [
			{	IDZdravila : "Q1SX8Rkxp8AFgra2TaOjaiK0BvX", NazivZdravila : "Aspirin Zipp 500 mg zrnca",
				ZacetekUporabe : "2016-01-20", KonecUporabe : "2016-02-20",
				Zdravnik : "Dr. Kovačič", NavodilaZdravnika : "po potrebi"},
			{	IDZdravila : "BUwMwCC4mGuoiF7WdIuzLe8VfAF", NazivZdravila : "LEKADOL 1000 mg tablete",
				ZacetekUporabe : "2016-02-20", KonecUporabe : "2016-03-20",
				Zdravnik : "Dr. Kovačič", NavodilaZdravnika : "po potrebi"},
			{	IDZdravila : "2MEVCXizEeg0X9ZBCIotfRd0weM", NazivZdravila : "Septolete D z okusom mentola 1 mg pastile",
				ZacetekUporabe : "2016-03-20", KonecUporabe : "2016-04-20",
				Zdravnik : "Dr. Kovačič", NavodilaZdravnika : "po potrebi"}
			]
			break;
	}
	
	for (i=0; i<meritve.length; i++) {
		dodajMeritve(ehrId, meritve[i].datumInUra, meritve[i].telesnaVisina, meritve[i].telesnaTeza, meritve[i].telesnaTemperatura, meritve[i].sistolicniKrvniTlak, meritve[i].diastolicniKrvniTlak, meritve[i].merilec)
	}
	for (i=0; i<zdravila.length; i++) {
		vnosZdravila(ehrId, zdravila[i].IDZdravila, zdravila[i].NazivZdravila, zdravila[i].ZacetekUporabe, zdravila[i].KonecUporabe, zdravila[i].Zdravnik, zdravila[i].NavodilaZdravnika)
	}

  return ehrId;
}

function generiraj3osebe() {
	$('#generiraj3osebe').modal('show');
	$("#generiraj3osebe-body").append("<p>oseba1: "+generirajPodatke(1)+"</p>");
	$("#generiraj3osebe-body").append("<p>oseba2: "+generirajPodatke(2)+"</p>");
	$("#generiraj3osebe-body").append("<p>oseba3: "+generirajPodatke(3)+"</p>");
	$("#generiraj3osebe-loading").html("");
}

function kreirajEHR(ime, priimek, datumRojstva){
	ehrId = "";
	sessionId = getSessionId();
	$.ajaxSetup({
		async: false,
		headers: {"Ehr-Session": sessionId}
	});
	var data = JSON.parse($.ajax({
		async: false,
		url: baseUrl + "/ehr",
		type: 'POST'
	}).responseText);
	
	var ehrId = data.ehrId;
	var partyData = {
		firstNames: ime,
		lastNames: priimek,
		dateOfBirth: datumRojstva,
		partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	};
	var party = JSON.parse($.ajax({
		async: false,
		url: baseUrl + "/demographics/party",
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(partyData)
	}).responseText);
	
	if (party.action == 'CREATE') {
		console.log("Uspešno kreiran EHR '" + ehrId + "'");
	}
	return ehrId;
}

function dodajMeritve(ehrId, datumInUra, telesnaVisina, telesnaTeza, telesnaTemperatura, sistolicniKrvniTlak, diastolicniKrvniTlak, merilec) {
	sessionId = getSessionId();
	var podatki = {
		"ctx/language": "en",
		"ctx/territory": "SI",
		"ctx/time": datumInUra,
		"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		"vital_signs/body_temperature/any_event/temperature|unit": "°C",
		"vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		"vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
	};
	var parametriZahteve = {
		ehrId: ehrId,
		templateId: 'Vital Signs',
		format: 'FLAT',
		committer: merilec
	};
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
		url: baseUrl + "/composition?" + $.param(parametriZahteve),
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(podatki),
		error: function(err) {
			console.log("Napaka '" + JSON.parse(err.responseText).userMessage) + "'";
		}
	});
}

function vnosZdravila(ehrId, IDZdravila, NazivZdravila, ZacetekUporabe, KonecUporabe, Zdravnik, NavodilaZdravnika) {
	sessionId = getSessionId();	
	var podatki = {
		"ctx/language": "en",
		"ctx/territory": "SI",
		"ctx/time": "2016-01-20T09:00Z",
		"medications/medication_instruction:0/order:0/comment:0": IDZdravila,
		"medications/medication_instruction:0/order:0/medicine": NazivZdravila,
		"medications/medication_instruction:0/order:0/directions": NavodilaZdravnika,
		"medications/medication_instruction:0/order:0/medication_timing/start_date": ZacetekUporabe,
		"medications/medication_instruction:0/order:0/medication_timing/stop_date": KonecUporabe,
		"medications/medication_instruction:0/order:0/timing": "R0/2000-00-00T00:00/P0",
		"medications/medication_instruction:0/narrative": Zdravnik
	};
	var parametriZahteve = {
		ehrId: ehrId,
		templateId: 'Medications',
		format: 'FLAT'
	};
	
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
		url: baseUrl + "/composition?" + $.param(parametriZahteve),
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(podatki),
		success: function (res) {
			$("#dodajZdravilo-IDZdravila").val("");
			$("#dodajZdravilo-NazivZdravila").val("");
			$("#dodajZdravilo-ZacetekUporabe").val("");
			$("#dodajZdravilo-KonecUporabe").val("");
			$("#dodajZdravilo-Zdravnik").val("");
			$("#dodajZdravilo-NavodilaZdravnika").val("");
		},
		error: function(err) {
			console.log("Napaka '" + JSON.parse(err.responseText).userMessage) +"'";
		}
	});
}




// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function izpisiEHRBolnika(ehrId) {
	sessionId = getSessionId();
	$('#vnos-EHR-ID').hide();
	$('#EHR-placeholder').hide();
	$('#nov-bolnik').hide();
	$('#vzorcne-osebe').hide();
	$('#EHR-podatki').slideDown('slow');

	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function (data) {
			var party = data.party;
			$("#EHR-podatki-ID").attr("placeholder", ehrId);
			$("#EHR-podatki-Ime").attr("placeholder", party.firstNames);
			$("#EHR-podatki-Priimek").attr("placeholder", party.lastNames);
			$("#EHR-podatki-DatumRojstva").attr("placeholder", new Date(party.dateOfBirth).toLocaleString());
		},
		error: function(err) {
			$('#EHR-podatki').hide();
			$('#EHR-placeholder').show();
			alert('Napaka: ' + JSON.parse(err.responseText).userMessage) +"'";
		}
	});
	
	document.getElementById("preberiTipZaVitalneZnake").selectedIndex = 0;
	$("#preberiTipZaVitalneZnake").trigger("change"); 
	$("#pregled-zdravil-body").hide();
}

function izberiEHRID() {
	var ehrId = $("#preberiEHRid").val();
	
	if (!ehrId || ehrId.trim().length == 0) {
		alert("Prosim vnesite EHR ID!");
	} else {
		izpisiEHRBolnika(ehrId);
	}
}

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		alert("Prosim vnesite zahtevane podatke");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    alert("Uspešno kreiran EHR '" + ehrId + "'");
		                    izpisiEHRBolnika(ehrId);
		                }
		            },
		            error: function(err) {
		            	alert("Napaka '" + JSON.parse(err.responseText).userMessage) +"'";
		            }
		        });
		    }
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	var ehrId = $("#EHR-podatki-ID").attr("placeholder");
	
	if (!ehrId || ehrId.trim().length == 0) {
		alert("Prosim izberite EHR ID!");
	} else {
		var datumInUra = $("#dodajVitalnoDatumInUra").val();
		var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
		var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
		var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
		var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
		var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
		var merilec = $("#dodajVitalnoMerilec").val();
		var podatki = {
			"ctx/language": "en",
			"ctx/territory": "SI",
			"ctx/time": datumInUra,
			"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
			"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
			"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
			"vital_signs/body_temperature/any_event/temperature|unit": "°C",
			"vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
			"vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		var parametriZahteve = {
			ehrId: ehrId,
			templateId: 'Vital Signs',
			format: 'FLAT',
			committer: merilec
		};
		
		$.ajaxSetup({
			headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
			url: baseUrl + "/composition?" + $.param(parametriZahteve),
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(podatki),
			success: function (res) {
				$("#dodajVitalnoDatumInUra").val("");
				$("#dodajVitalnoTelesnaVisina").val("");
				$("#dodajVitalnoTelesnaTeza").val("");
				$("#dodajVitalnoTelesnaTemperatura").val("");
				$("#dodajVitalnoKrvniTlakSistolicni").val("");
				$("#dodajVitalnoKrvniTlakDiastolicni").val("");
				$("#dodajVitalnoMerilec").val("");
				alert("Podatki so bili uspešno vnešeni.\n(" + res.meta.href + ")");
			},
			error: function(err) {
				alert("Napaka '" + JSON.parse(err.responseText).userMessage) + "'";
			}
		});
	}
}

function preberiMeritveVitalnihZnakov() {
	$("#preberiMeritveVitalnihZnakovSporocilo").html("");
	var tip = $("#preberiTipZaVitalneZnake").val();
	if (tip = "" || !tip || tip.trim().length == 0) {
		$("#pregled-meritev-body").hide();
		return;
	}
	
	sessionId = getSessionId();
	var ehrId = $("#EHR-podatki-ID").attr("placeholder");
	tip = $("#preberiTipZaVitalneZnake").val();
	
	if (!ehrId || ehrId.trim().length == 0) {
		alert("Prosim izberite EHR ID!");
	} else {
		$("#pregled-meritev-body").show();
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "telesna višina & telesna teža") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (visina) {
					    	if (visina.length > 0) {
								
									$.ajax({
										url: baseUrl + "/view/" + ehrId + "/" + "weight",
										type: 'GET',
										headers: {"Ehr-Session": sessionId},
										success: function (teza) {
											if (teza.length > 0) {
												
												drawChartVisinaTeza(visina, teza, "Telesna višina [cm]", "Telesna teža [kg]", "Graf telesne višine in telesne teže");
												
											} else {
												$("#preberiMeritveVitalnihZnakovSporocilo").html(
									"<span class='obvestilo label label-warning fade-in'>" +
									"Ni podatkov!</span>");
											}
										},
										error: function() {
											$("#preberiMeritveVitalnihZnakovSporocilo").html(
								  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
								  JSON.parse(err.responseText).userMessage + "'!");
										}
									});

					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "krvni tlak") {
						$.ajax({
							url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
							type: 'GET',
							headers: {"Ehr-Session": sessionId},
							success: function (tlak) {
								if (tlak.length > 0) {
									
									drawChartVisinaTeza(tlak, null, "Sistolični krvni tlak [mm(Hg)]", "Diastolični krvni tlak [mm(Hg)]", "Graf sistoličnega in diastoličnega krvnega tlaka");
									
								} else {
									$("#preberiMeritveVitalnihZnakovSporocilo").html(
						"<span class='obvestilo label label-warning fade-in'>" +
						"Ni podatkov!</span>");
								}
							},
							error: function() {
								$("#preberiMeritveVitalnihZnakovSporocilo").html(
					  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
					  JSON.parse(err.responseText).userMessage + "'!");
							}
						});
				} else if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + new Date(res[i].time).toLocaleString() +
                          "</td><td class='text-right'>" + res[i].temperature +
                          " " + res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").html(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} 
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

function isciZdravila() {
	var zdraviloQuery = encodeURI($("#iskanoZdravilo").val());
	if (zdraviloQuery = "" || !zdraviloQuery || zdraviloQuery.trim().length == 0) {
		alert("Vpišite ime zdravila");
		return;
	}
	
	zdraviloQuery = encodeURI($("#iskanoZdravilo").val());
	$("#seznamZdravil .row").html('<div class="text-center" style="margin-top:10px"><h1><em>Nalaganje . . .</em></h1></div>');
	$.ajax({
	  method: "GET",
	  url: "https://cors-anywhere.herokuapp.com/https://mediately.co/si/drugs/?q=" + zdraviloQuery,
	  success: function (res) {
		var body = res.split("window.preload = ")[1].split("</script>")[0];
		var content = eval(body);
		var products = content[1][4].drugs;
		var zdravila = [];
		$(products).each(function(index) {
			zdravila[index] ={
				id : this.uuid,
				naziv : this.registered_name
			}
		});
		izpisiZdravila(zdravila);
	  },
		error: function(err) {
			alert("Napaka '" + JSON.parse(err.responseText).userMessage) +"'";
		}
	})
}

function izpisiZdravila(zdravila) {
	$("#seznamZdravil .row").html("");
	$("#seznamZdravil .row").append('<div class="list-group">');
	$(zdravila).each( function() {
		var html = '<button type="button" class="list-group-item" id="' + this.id + '" onclick="izberiZdravilo(this)">' + this.naziv + '</button>';
		$("#seznamZdravil .row").append(html);
	});
	$("#seznamZdravil .row").append('</div>');
}

function izberiZdravilo(el) {
	$("#dodajZdravilo-IDZdravila").val(el.id);
	$("#dodajZdravilo-NazivZdravila").val($(el).text());
	$('#izberiZdravilo').modal('hide')
}

function vnesiZdravilo() {
	sessionId = getSessionId();
	var ehrId = $("#EHR-podatki-ID").attr("placeholder");
	
	if (!ehrId || ehrId.trim().length == 0) {
		alert("Prosim izberite EHR ID!");
	} else {
		var IDZdravila = $("#dodajZdravilo-IDZdravila").val();
		var NazivZdravila = $("#dodajZdravilo-NazivZdravila").val();
		var ZacetekUporabe = $("#dodajZdravilo-ZacetekUporabe").val();
		var KonecUporabe = $("#dodajZdravilo-KonecUporabe").val();
		var Zdravnik = $("#dodajZdravilo-Zdravnik").val();
		var NavodilaZdravnika = $("#dodajZdravilo-NavodilaZdravnika").val();
		
		var podatki = {
			"ctx/language": "en",
			"ctx/territory": "SI",
			"ctx/time": "2016-01-20T09:00Z",
			"medications/medication_instruction:0/order:0/comment:0": IDZdravila,
			"medications/medication_instruction:0/order:0/medicine": NazivZdravila,
			"medications/medication_instruction:0/order:0/directions": NavodilaZdravnika,
			"medications/medication_instruction:0/order:0/medication_timing/start_date": ZacetekUporabe,
			"medications/medication_instruction:0/order:0/medication_timing/stop_date": KonecUporabe,
			"medications/medication_instruction:0/order:0/timing": "R0/2000-00-00T00:00/P0",
			"medications/medication_instruction:0/narrative": Zdravnik
		};
		/*
		var podatki = {
		  "ctx/language": "en",
		  "ctx/territory": "SI",
		  "medications/medication_instruction:0/order:0/medicine": "Medicine 4",
		  "medications/medication_instruction:0/order:0/directions": "Directions 37",
		  "medications/medication_instruction:0/order:0/medication_timing/start_date": "2017-05-28T09:10:27.066Z",
		  "medications/medication_instruction:0/order:0/medication_timing/stop_date": "2017-05-28T09:10:27.066Z",
		  "medications/medication_instruction:0/order:0/comment:0": "Comment 57",
		  "medications/medication_instruction:0/order:0/timing": "R0/2000-00-00T00:00/P0",
		  "medications/medication_instruction:0/narrative": "Human readable instruction narrative"
		}
		*/
		var parametriZahteve = {
			ehrId: ehrId,
			templateId: 'Medications',
			format: 'FLAT'
		};
		
		$.ajaxSetup({
			headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
			url: baseUrl + "/composition?" + $.param(parametriZahteve),
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(podatki),
			success: function (res) {
				$("#dodajZdravilo-IDZdravila").val("");
				$("#dodajZdravilo-NazivZdravila").val("");
				$("#dodajZdravilo-ZacetekUporabe").val("");
				$("#dodajZdravilo-KonecUporabe").val("");
				$("#dodajZdravilo-Zdravnik").val("");
				$("#dodajZdravilo-NavodilaZdravnika").val("");
				alert("Podatki so bili uspešno vnešeni.\n(" + res.meta.href + ")");
			},
			error: function(err) {
				alert("Napaka '" + JSON.parse(err.responseText).userMessage) +"'";
			}
		});
	}
}

function preberiZdravila() {
	sessionId = getSessionId();
	var ehrId = $("#EHR-podatki-ID").attr("placeholder");
	if ($("#pregled-zdravil-body").css('display') != 'none' )  {
		$("#pregled-zdravil-body").hide();
	} else if (!ehrId || ehrId.trim().length == 0) {
		$("#pregled-zdravil-body").hide();
		alert("Prosim izberite EHR ID!");
	} else {
		$("#pregled-zdravil-body table").html("");
		$("#pregled-zdravil-body").slideDown('slow');
		var AQL = "SELECT " +
			"a/content[openEHR-EHR-INSTRUCTION.medication.v1]/activities[at0001]/description[at0002]/items[at0003]/value/value as NazivZdravila, " +
			"a/content[openEHR-EHR-INSTRUCTION.medication.v1]/activities[at0001]/description[at0002]/items[at0035]/value/value as IDZdravila, " +
			"a/content[openEHR-EHR-INSTRUCTION.medication.v1]/activities[at0001]/description[at0002]/items[at0009]/value/value as NavodilaZdravnika, " +
			"a/content[openEHR-EHR-INSTRUCTION.medication.v1]/activities[at0001]/description[at0002]/items[at0010]/items[at0012]/value/value as ZacetekUporabe, " +
			"a/content[openEHR-EHR-INSTRUCTION.medication.v1]/activities[at0001]/description[at0002]/items[at0010]/items[at0013]/value/value as KonecUporabe, " +
			"a/content[openEHR-EHR-INSTRUCTION.medication.v1]/narrative/value as Zdravnik " +
			
			"from EHR e " + 
			"contains COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] " +
			
			"where " +
				"a/name/value='Medications' and " +
				"e/ehr_id/value='"+ ehrId + "' " +
			"ORDER BY a/content[openEHR-EHR-INSTRUCTION.medication.v1]/activities[at0001]/description[at0002]/items[at0010]/items[at0012]/value/value DESC " +
			"offset 0 limit 100 ;";
		$.ajax({
			url: baseUrl + "/query?" + $.param({"aql": AQL}),
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
			success: function (res) {
				if (res != null && res.resultSet.length > 0) {
					var results = 
					'<table class="table table-hover">\
						<thead><tr>'+
						'<th style="min-width: 180px;">Naziv zdravila</th>'+
						'<th style="min-width: 250px;">Navodila zdravnika</th>'+
						'<th style="min-width: 50px;">Zdravnik</th>'+
						'<th style="min-width: 100px;">Zacetek uporabe</th>'+
						'<th style="min-width: 100px;">Konec uporabe</th>'+
						'</tr></thead>\
						<tbody>';
					for (var i in res.resultSet) {
						results +=
						'<tr id="'+res.resultSet[i].IDZdravila+'" onclick="zdraviloPodrobnosti(this)">'+
						'<td>'+res.resultSet[i].NazivZdravila+'</td>'+
						'<td>'+res.resultSet[i].NavodilaZdravnika+'</td>'+
						'<td>'+res.resultSet[i].Zdravnik+'</td>'+
						'<td>'+res.resultSet[i].ZacetekUporabe+'</td>'+
						'<td>'+res.resultSet[i].KonecUporabe+'</td></tr>';
					}
					results += "</table>";
					$("#pregled-zdravil-body").html(results);
				} else {
					$("#pregled-zdravil-body").html(
		"<span class='obvestilo label label-warning fade-in'>" +
		"Ni podatkov!</span>");
				}
			},
			error: function(err) {
				alert("Napaka '" + JSON.parse(err.responseText).userMessage) +"'";
			}
		});
	}
}

function zdraviloPodrobnosti(el) {
	$('#zdraviloPodrobnosti').modal('show');
	
	$.ajax({
	  method: "GET",
	  url: "https://cors-anywhere.herokuapp.com/https://mediately.co/si/drugs/" + el.id,
	  success: function (res) {
		var body = res.split("window.preload = ")[1].split("</script>")[0];
		var content = eval(body);
		var podrobnosti = content[1][6];
		izpisiPodrobnostiZdravila(podrobnosti)
	  },
		error: function(err) {
			alert("Napaka '" + JSON.parse(err.responseText).userMessage) +"'";
		}
	})
}

function izpisiPodrobnostiZdravila(podrobnosti) {
	povezava = '&nbsp;&nbsp;<a href="https://mediately.co/si/drugs/'+podrobnosti.uuid+'" target="_blank"><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>';
	$('#zdraviloPodrobnosti-title').html(podrobnosti.registered_name + povezava);
	var html = '<div class="container-fluid">\
	<p><a href="'+ podrobnosti.pil +'" target="_blank" title="Navodilo za uporabo">\
	<img src="https://mediately.co/frontend/images/pdf.svg" width="22" height="22">Navodilo za uporabo</a></p>\
	<p class="text-info"><strong>SESTAVA: </strong> <span> '+ podrobnosti.ingredients +'</span></p>\
	<p class="text-info"><strong>INDIKACIJE: </strong> <span></p> '+ podrobnosti.indications +'</span>\
	<p class="text-info"><strong>ODMERJANJE: </strong> <span></p> '+ podrobnosti.dosing +'</span>\
	<p class="text-info"><strong>KONTRAINDIKACIJE: </strong> <span></p> '+ podrobnosti.contraindications +'</span>\
	<p class="text-info"><strong>POSEBNA OPOZORILA: </strong> <span></p> '+ podrobnosti.warnings +'</span>\
	<p class="text-info"><strong>NEŽELENI UČINKI: </strong> <span></p> '+ podrobnosti.undesirable +'</span>\
	</div>'
	
	$('#zdraviloPodrobnosti-body').html(html);
}

function drawChartVisinaTeza(podatkiA, podatkiB, labelaA, labelaB, NaslovGrafa) {
  var podatki = [];
  var datumi = [];
  if (podatkiB != null) {
	for (var i=0; i<podatkiA.length; i++) {
		podatki.push([new Date(podatkiA[i].time), podatkiA[i].height, podatkiB[i].weight]);
		datumi.push(new Date(podatkiA[i].time));
	}
  } else {
	for (var i=0; i<podatkiA.length; i++) {
		podatki.push([new Date(podatkiA[i].time), podatkiA[i].systolic, podatkiA[i].diastolic]);
		datumi.push(new Date(podatkiA[i].time));
	}
  }
  
  var chartDiv = document.getElementById('rezultatMeritveVitalnihZnakov');

  var data = new google.visualization.DataTable();
  data.addColumn('date', 'Datum');
  data.addColumn('number', labelaA);
  data.addColumn('number', labelaB);

  data.addRows(podatki);


  var classicOptions = {
	title: NaslovGrafa,
	// Gives each series an axis that matches the vAxes number below.
	series: {
	  0: {targetAxisIndex: 0},
	  1: {targetAxisIndex: 1}
	},
	vAxes: {
	  // Adds titles to each axis.
	  0: {title: labelaA},
	  1: {title: labelaB}
	},
	hAxis: {
	  ticks: datumi
	},
	
  };

  var classicChart = new google.visualization.LineChart(chartDiv);
  classicChart.draw(data, classicOptions);
}

$(document).ready(function() {
    google.charts.load('current', {'packages':['line', 'corechart'], 'language': 'sl'});
});